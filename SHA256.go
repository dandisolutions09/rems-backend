package main

import (
	// "crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	// "fmt"
)

func hashPassword(password string, salt []byte) (string, []byte, error) {
	// Concatenate password and salt
	passwordSalt := append([]byte(password), salt...)

	// Hash the concatenated value using SHA-256
	hash := sha256.New()
	hash.Write(passwordSalt)
	hashedPassword := hex.EncodeToString(hash.Sum(nil))

	return hashedPassword, salt, nil
}



func comparePasswords(inputPassword string, storedHashedPassword string, storedSalt []byte) bool {
	// Hash the input password using the stored salt
	hashedInputPassword, _,  err := hashPassword(inputPassword, storedSalt)
	if err != nil {
		fmt.Println("Error hashing input password:", err)
		return false
	}

	// Compare the hashed input password with the stored hashed password
	return hashedInputPassword == storedHashedPassword
}

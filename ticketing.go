package main

import (
	"context"
	"encoding/json"
	"fmt"

	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Ticket struct {
	ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	TicketNumber string             `json:"ticket_number" bson:"ticket_number"`
	Issue        string             `json:"issue" bson:"issue"`
	Description  string             `json:"description" bson:"description"`
	Status       string             `json:"status" bson:"status"`
	CreatedAt    string             `json:"created_at" bson:"created_at"`
}

func handleAddTicket(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	//fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	//fmt.Printf(decoder)
	var data Ticket
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Update createdAt with the current time
	//data.CreatedAt, err = time.Parse(time.RFC3339, time.Now().Format("2006-01-02 15:04:05"))

	fmt.Print("created at:", data.CreatedAt)

	currentTime := time.Now()
	//timeString := currentTime.Format("2006-01-02 15:04:05")
	timeString := currentTime.Format("Monday, 02 January 2006 15:04:05")
	//fmt.Println("Current time as string:", timeString)

	data.CreatedAt = timeString

	//subject := "Ticket#" + ":-"+data.Issue
	body := data.Issue

	newTicket, _ := SendMail(data.Issue, body, data.Description)

	data.TicketNumber = fmt.Sprint(newTicket)
	data.Status = "UNRESOLVED"

	// fmt.Println("username:-->", data.Username)
	// fmt.Println("user role:-->", data.UserRole)
	// fmt.Println("incoming log:-->", data)

	//utcTime, err := time.Parse(time.RFC3339, timestamp)

	// if err != nil {
	// 	fmt.Print("Failed to convert time")

	// }

	// utcTime, err := time.Parse(time.RFC3339, utcTimeString)

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(tickets_collection)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	// fmt.Fprintf(w, "Nurse inserted Successfully")

	response := map[string]string{"message": "Ticket has been added successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleGetTickets(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(tickets_collection)

	// Define a slice to store retrieved facilities
	var tickets []Ticket

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving nurses", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var ticket Ticket
		if err := cursor.Decode(&ticket); err != nil {
			http.Error(w, "Error decoding driver", http.StatusInternalServerError)
			return
		}
		tickets = append(tickets, ticket)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(tickets)
}

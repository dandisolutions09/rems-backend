// package main

// import (
// 	"bytes"
// 	"crypto/aes"
// 	"crypto/cipher"
// 	"encoding/base64"
// 	"fmt"
// )

// // GetAESDecrypted decrypts given text in AES 256 CBC
// func GetAESDecrypted(encrypted string) ([]byte, error) {
// 	key := "my32digitkey12345678901234567890"
// 	iv := "my16digitIvKey12"

// 	ciphertext, err := base64.StdEncoding.DecodeString(encrypted)

// 	if err != nil {
// 		return nil, err
// 	}

// 	block, err := aes.NewCipher([]byte(key))

// 	if err != nil {
// 		return nil, err
// 	}

// 	if len(ciphertext)%aes.BlockSize != 0 {
// 		return nil, fmt.Errorf("block size cant be zero")
// 	}

// 	mode := cipher.NewCBCDecrypter(block, []byte(iv))
// 	mode.CryptBlocks(ciphertext, ciphertext)
// 	ciphertext = PKCS5UnPadding(ciphertext)

// 	return ciphertext, nil
// }

// // PKCS5UnPadding  pads a certain blob of data with necessary data to be used in AES block cipher
// func PKCS5UnPadding(src []byte) []byte {
// 	length := len(src)
// 	unpadding := int(src[length-1])

// 	return src[:(length - unpadding)]
// }

// // GetAESEncrypted encrypts given text in AES 256 CBC
// func GetAESEncrypted(plaintext string) (string, error) {
// 	key := "my32digitkey12345678901234567890"
// 	iv := "my16digitIvKey12"

// 	var plainTextBlock []byte
// 	length := len(plaintext)

// 	if length%16 != 0 {
// 		extendBlock := 16 - (length % 16)
// 		plainTextBlock = make([]byte, length+extendBlock)
// 		copy(plainTextBlock[length:], bytes.Repeat([]byte{uint8(extendBlock)}, extendBlock))
// 	} else {
// 		plainTextBlock = make([]byte, length)
// 	}

// 	copy(plainTextBlock, plaintext)
// 	block, err := aes.NewCipher([]byte(key))

// 	if err != nil {
// 		return "", err
// 	}

// 	ciphertext := make([]byte, len(plainTextBlock))
// 	mode := cipher.NewCBCEncrypter(block, []byte(iv))
// 	mode.CryptBlocks(ciphertext, plainTextBlock)

// 	str := base64.StdEncoding.EncodeToString(ciphertext)

// 	return str, nil
// }

package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"strings"
)

func encryptAES(data []byte, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// Pad the data if needed
	blockSize := block.BlockSize()
	data = pkcs7Pad(data, blockSize)

	ciphertext := make([]byte, blockSize+len(data))
	iv := ciphertext[:blockSize]

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(ciphertext[blockSize:], data)

	return ciphertext, nil
}

func decryptAES(ciphertext []byte, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	blockSize := block.BlockSize()
	iv := ciphertext[:blockSize]
	ciphertext = ciphertext[blockSize:]

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(ciphertext, ciphertext)

	// Unpad the data
	ciphertext = pkcs7Unpad(ciphertext)

	return ciphertext, nil
}

func pkcs7Pad(data []byte, blockSize int) []byte {
	padding := blockSize - len(data)%blockSize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(data, padText...)
}

func pkcs7Unpad(data []byte) []byte {
	padding := int(data[len(data)-1])
	return data[:len(data)-padding]
}

func encryption(w http.ResponseWriter, r *http.Request) {
	// Example Base64-encoded image with data URI prefix
	base64ImageURI := "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaAAAACWCAYAAACYYlxdAAAAAXNSR0IArs4c6QAAH3RJREFUeF7tnQe4PkV1xt+JiSWKUSNoNHZsIRbsRI3BEhALURSjWIMJiUTsNSJgi72gEEUUUEAUFJSoiFHsxCCxBRvG2EsssZeYZLK/ubN/9iv33q/ttzvf957nuc/Dn7s7c+advXt2Zt7zniCbETACRsAIGIEOEAgd9Okut0QgXlXSoZKuIumLkn4s6SgpfNfAGQEjYARWCQEHoF7NZjxO0kPHuETwOV3SxyWdIYVv98ptO2MEjIARmAEBB6AZQGvnlniUpEdM2PaFkg6TwhsmvN6XGQEjYAR6h4ADUC+mJF5J0iyrmg9IeoUUTu3FMOyEETACRmAKBByApgCrvUvjX0l6VaP9n0j6lKRPTrgqOlvSBVJ4bHs+umUjYASMwGIRcABaLJ4ztjZy9nO8FB52UWPxTyQ9RNJtJV1D0sU36egCSaeZtDDjNPg2I2AEloqAA9BS4d6ss5EAdHTFejt4/NVxJ0k3zyuj+27SIqSFEyWdJIXzezFEO2EEjIARGELAAagXj0SMQ24cIYXDt3ct3lQSgWqfTNsedwsB6GRJ39xg0YXPb9+urzACRsAItI+AA1D7GG/TQ1rRkOvTtNtJ4cOTuxYvIWmPKm/ogZIO3OY+AhJnTB+T9HYpvG/yfnylETACRmBxCDgALQ7LGVuK5P2Q/9O0naTw09kajDtXhAS25u4u6a4TtnFmtYI6RtIHKiLDcDCcsAlfZgSMgBGYDgEHoOnwauHq+DZJ92g0TKLpvRbTUdxV0g0k7alqT0/S7rnda0riZ9i+JOktUnjCYvp3K0bACBiBzRFwAOr86Zj1/Gdex+N1KnWFm1RnQ1DA2b677FCLbM29QNKHvCqaF2vfbwSMwDgEHIA6fS4SvfqcIRf2XP65TNq2g8xwB0n41DS25I7fkALyeVGnj4s7NwIrhoADUKcTGmG6HTboQuh4TiLbf3tLuo+kKwzB84OcIPv+KlC9zwGp04fHnRuB4hHo+GVXPH5zDmAkAPFS57ymB7blqqj278uS3iwJwoQDUg9mzS4YgZIQcADqdLYi8jucwdR2qhT279SlTTuPe0m65wTSQJwdHSnpPCl8vZ9jsVdGwAj0AQEHoE5nIf5nJZvD+Utt+5chLJrOrtieu3VOgCX/aJyRb/RZSedKeoYU2MKzGQEjYAQSAg5AnT0I4wgIXZ//zANGGg8/EBlIjL2hpMsNtXiepG9I+mD1+zdKgf+2GQEjsKYIOAB1NvEjCtiHS+GIztxZeMdxt4qwcG9Jt5d0l02aR43hdVmRgRwkmxEwAmuEgANQZ5MdoV83Kc+7Vec/n+nMnVY7TsEIcsWd88+lx3RHPSTOjF6SV0f/26pLbtwIGIHOEXAA6mwKBhJQX1RJ7zy+M1eW3nG8VVVaHCHVP6vkf+6Yt+yGvXjvxuoonLB099yhETACS0HAAWgpMA93EtmWopop9nkpIJezphYvk1dHbNdBZvjNISD+K7PqPiqFd64pSB62EVhJBByAlj6tEfbY9xvddqB8sPRBT9hhZGuOVdEDcomJ5n0w6j5SadudIgWUGWxGwAgUjoAD0NInMD6m0l17ce72TCmQW2MbQSBS+fU2kv56jDwQpSpg0b3cwBkBI1AuAg5AS527SIkESh/UVkjez1JBGtNZSoJFMHVItkiUIH+hV0Rdz4/7NwKzIeAANBtuM94Vvybp9/PNW5TdnrH5tbgtHiUJtYgrNob7vSyW2lSVWAs0PEgjUDICDkBLm70B2jUH6zeTAlpqtqkRiNQyYivzkKFbwRMpoBMslDo1qL7BCCwdAQegpUAe35SrlNa9HeLzi0UAH6+aAxHsuSsNtQhpgWDkSq+LgNptGIEWEHAAagHUwSZHFK997rNwzCPF9G4r6SBJ+45pnppGH5V0oSQEXwlMNiNgBDpGwAGo9QkYSDg9Rgq8JG2tIZAqvT5R0vXGsOeavULl/kolBnuUFL7bmjtu2AgYgU0RcABq7eEYERs9otJ+pQCdbWkIxHtIeuw2gejnG8nAeptrGi1tYtyREUgIOAC18iDEa0t6buPcx8GnFZynaTSSb0W1VwgMw2XHhxtii46qr5TLYMvOK6RpoPa1RmBCBByAJgRq8ssiUjJs7xyQ7+lxkbnJR7VaV0ao8JSNePgEwYih1wHJ23Wr9SB4NB0j4AC08AmIf1SVHyBTP1vJNX4WDk5PG4x3k/TgnF+0lY8w687fWN2Gd/V0MC25FU/KEkm0f7wUHtZSR252jRBwAFroZCedN7ZtLpabvakUPrnQLtxYiwhEiAtI/5BjNIl9bkMSKKl2r3g9o/grSRf3h9Ukj4WvmRQBB6BJkZrouvgpSTfKl95HCm+e6DZf1EME4iMyrfvGEzpHEiz5Xu9cPZp3vLkkigc27VpOpJ7wyfBlmyLgALSwhyO+QFJd0+dEKTxoYU27oSUhkM7v7lSVDf/LDaUKXWvGjr8g6bgNZt0qFBmMT5D0/AYWF0qB1aLNCMyFgAPQXPDVN0dKCJye/wWl905S+MZg0/Gu+Yxhp2rLBpbcF/Pvd24wrqh5wxmDbakIRAINK579GkHnP3Ke0KmSdslEBIrowaQjOFHHaBIj1whSypvKDUbx3bmSbT3ek6VQk2wmwcDXGIGxCDgAzf1gxF1zcbnfk0QZ6dtL4dyNZiMvLP5QUcGetOgcAYhidZQbIHvf1hoCkTMNSoATfLBvSfpnSZR5+IAUtigLnvToDm6seifxEjYdlPzClBgiNPSm+KvTCiaZbV+zLQIOQNtCtN0FAyKjbMN9RtJDJqT3btf4JyS9NbOOLFy6HVpT/T7lBb0s5wV9ulIpP1YKR07VxI6L0xnJ32x8fCQFhq2MeUQstZCk5EhNpvxBtWNYDkCzPSi+awgBB6C5HolIsumTchPojF13i+b4imQ7Byov2mWxkoFh+42f7RIjaZZtnPdI4cS5XPbNQP+KvHpBlfxRVdBgS2mL1c40oCUFDMpC3H+Lu+j3VlKot2Gn6WCJ16ax8DH10KFOryaFry/REXe1ogg4AM08sfFPJZ0h6VJbNEHQ+E5mRpFZv4VFAhGqzgdWgWq3LS4kiD1HCqyObFMhEAkKx0q6RFaqeLYUfjFVE1NdnLbpeIlT3RVSCjp1tRWgiB4/lEVeh0e9i9UhpnoQfPEmCDgAzfRoxMtJ+qykKw/dDvGA/X22c+bY54/1VydnSPQ1bIVt48wE8gJvSni+MJ9jkEB64ChJZIHdjW0qQudu5oQ9rJ+VXNN2Ihp6nFvy3+PsOquf99T28+D2QcABaOrnIG1LPG9jC2XAEL08c7HbKhFiA+3CsoPsMGxHSDpSCj+Yehgrf0P83UwAQRmbukGc81DS+wwpsP3ZgQ0oo/fsHCWVDSFQs2rbyr4shVnp6R1g7i77jIAD0FSzE9kLf/qY/JA9pAB7qkWLBL27VKSE3Yc6IRP/lfOtuFp0e+lNR5QMbpexgj79TxslF/R2Kfx66e4MdNi3ABRvLQnpKJQfrjYGG84tYWKyGqrNAajbh2ilencAmng6I4mFw4exp0hhq8PmiVuf7MK0+oIyfN+h63nJUmuI86E1syQsypbRPvllihwSAYezt9Ol8G/9AWSgOCHbtCS8dmDpOWI1OI78krd3E/PtvFz2nGtrc02rDmZsVbt0ANp2ZtMf670lPXLo0pdIge2xDixRiJ9WfdXfcqjzJ0iBs44VtlRwbi9Je+SVTr1lhAAspA+Czvf7CUB8Zp433CPnCDkbNNaWYOk5RnB1zzHbbOSeQcY4dHQlPZBmgJ892zpcAnTuojUEHIC2hDaxmJAgGV5xnCOFO7Y2KxM3HMk7IWm1aSukVByvL4kfgg7qA7w8Oc+pjRyp12Z6+s8mhq2zC9PzhMJCbXeTwjvadSfy7P5FlWC791A/UMDfsr12nQNQu/Oz3q07AG06/yNF5eorkVZB7eBr/Xh00guGujbQwmt7nxR4WRdkkQBD0iOU5ZtkBlYz+56xIIjJdiNssvdKAeXxwmxgK/cCKfzh4gcQ/yBvr7EVOWxH5yJ7E7I048cr4gZszNr29Hnj4mdsXVt0ABo780le5zljVj7/I2nf9r9ap30cE1uOrP7mSo0XzQv6q1icSlfcJzP80Mkbtg9W5zr/kreqzpXCR6ZFpZ/Xj6yCFrSlFS+dt2QhqRwytM3G2SBnTmdPj0lklYT+HfYlKTRzmaZvzncYgQYCDkAjj0OS4X/AUAJeTds9rlr5kCjaQ0uH8a8fOlgm+fVB/VitRURYYaexMmO1xiqnNs5vYFv964YWW/j3HgK8QJci6hmoaGAoZu87X+MRkgDJy80PECj677pIl3DWHiLBiw8FjA8aaO02I7AQBByABmAcYCnVv2HLjW0hNN52l8J/LwT51hoZkAeiF+jbZ3W3bRIhcED3/fPqvObqedgUb3tPFeRRWaZ+zk9bg6O3DQ9Qsu8nBWoJTWERBQ6qkqKwAK5XaZQOJx9tQarqkW3POiG1p8mzU8DmS3uFgAPQjukYOWzlN2wBcTaBTthNKso1pRYKsIHaRPjLFhb5HkuytCW4v6SnSLpS7pTV2NsknSaFry7JkR53k5iMkCiwKXJrIiudx+UkW9S8oU3zc/Diyz0k5tw5DRB9/tPjJ6pE1xyA0qxFvj6HmW7kj/BVyVnF30mBM6GCLJETml/V5HU8td2VUEQRmoJu/CAhRNChKixJoCtesnqWRyOelmsQcfM9pPCP41tJZSNQwzioYj3W7EsETRFVfXV7W6wp8Zr8N+z8yr9bzDJK32MENkPAAUgDWww1Trysf5Spq7Ddri0FCAiFWSSD/cyG0+igHbbYOkORXCRovveriBCX32CnpWJ7SARdUBhgS3Z34CPhtaPni+lc79E5ARppIQzV9edJ4TXtOzvwYVYgs7J9hNzDfAiseQCK/BHz8mwaMvMUKXtRJbvzfxvZ9SUXhhs51yIo3H1+dlwk4LDFVpMJoPX+ff5S7mki6Hx/LO3cPbD1m7e4EiWd55JVLHJC5DidlVewlPtekg345gC0JNTXqZs1DkCpnAIrgqZ9M2+JHFNtHd0oU1c7kktZ5GM4sh0Hs4ntuCnr0aSzHUgFhzbOdsjLeYUU6vOMRTq+Bm0NnLN8Lj+TMDF/q1LtZvX9qlyQcKjE+zKgGSBKcMYEtd9mBBaGwJoGoE0VDqCuIo3Cec/Ps1RKgcmO456PkSB0dkXRRdJmQosUWeNliKGzBovtRVKAOm2bC4HI6qY5FwQi8roolPfjuZqe+eaR5+UG5ZBwZh60b1wyAusagF6aK2E24Sb4cKjLFhXbHqwQ2FJaIRt5qTxFCnU+yibjTOrS/9D4JXpmBB7OyGxzIRDZZqMMwrAS9QFSOHmupue+eeCD48NSIIfLZgQWisC6BqBmPRjOfF5TlUY6XEolD0i04wv0dv0VtZznGRhIgqShMdpxKcdkv5wsifbaL/MZxBOlwCG4bWYEIniSzAy5ANIGSbcvzh8+TXmcjinPA+c/C1JrmBk037iiCKxhAIqnZMZWPaX5jyteLyebXizL7ZCzsqI2QkxovGAiSbevzvV0GP8782rQJcDnehoSRR1WIgKyqELAFmRLEyUEAjzpACR8kvhZ226Lz+2ZdBAD5bh9/jMpbL5uKgTWLABFcilObyDUfPEiB0Oy5hulQNb+ils8MSczMk4Kj3HATL4JzDYM5YdnSoGAbZsJgURR5+yM5+kyGzJDCVco6s3y3I3WB7a++P8drYQi/lFGnGfj+lIg78hmBBaKwLoFIJJLySTHmsGnTtpEZmevdpM1Fzp/czSWxEBJEq2LkkE5/42LsEm6XwWUOJgDglZujZSrhmEJk40XOEbJ9JfnooEwLbexyOqbInu17b/8YoM78uNMv95uuvz7mRFYowAU2e7gaxRryOAnFWEYXeiVvVUKrJLWxOJDchG3erzo3t2rEsdEgt82FQLpbAdySy3cyd0oQRw1ffBIW3EnND6WaGuJOmypnEOdRHy0FA6eCgpfbAQmRGBNAtCA7la1+AmNcce/zV+nfKXecfOtkQkRLeKy9IJ5dpZ3GfZ4bykM50cVMapunIxo3h3ZyIv6XlYlR/NuzhISEcIHpUFqg6n5sva3wyJSSuR3Yc+QQrMkdzcwu9eVRGBdAlBTVHE/KVDjJNuOZDuoxY9fyVm+aKxIu1CyGxWD2l6Zy1vXigZjJGFWG5XpR5cC+AE5KfcG+X4qnXJehkzOgijqI7R5uloCIy1CuaYeE/ZoKZCTZDMCC0dgTQIQuKVtDVY/DZn6yKEwW29LVote+DxO0GDkhQnxoLaTNraMwsekSOVMzixq65B9NcFQOrskQlJ5cJbI4QwNY/sWJhuroJZsQJGAPlDqeEN7Z5XxbpJqYdSbrseuQEtT52a3RGCNAtAwDpGVALL22BZKxCU/QRFFagILuU2/k0dCiQm+ot9x0chGZPdPrei/bC3ZFCmZTc0dVo1Q1LFfSAK/p1WMSXLGWrbE3nzUULFB5JQ4n5mwtPY0Lg6oYF+9PbXtaXzytauIwJoGoIGtjWOkgMz9ClmkjARfsY+ttoXqLSLyedhm5It9jEUkh3bOv3AAUqRKKUw0EnIJ5BhJy6hCcA7TAUMwkrD6mMbktRSEIlp/z8h1sC4vhZ+s0B+Hh9IjBNY1ALFff82NCpKBEtErYhFJF5S87yrpt/OgKC1xeFWQ7uytBzmwDccL54ZS6EAAs8upSGcfBG3q3jTlcaCrnyiFM7r0Tkor1b2rFRglvWtDi4+KqlMKy241kh27A5QguVSZpUi6nSn3PhkCaxiAYh18pqhCORmY3VwVSXBET4wXU53jhIAl1VsPmpxSHdlmel1jDB3knnSBYGTVx2qRbcobNjz4VqZCv04Kn+3Cs837jE/OpS+alywwYXXHx8jPpMDzZTMCrSCwRgEo5fsgpFlvYexS5TeQ5V2oxetLunMez3Uag/h0DjysfKawERmYJbCtpnBv4ZfG3bPqQ7MSLgroFPBja+usbrbZJh3oCHGEZxnJHHyf0yIkB8qQ/FgK9dnhnG36diMwisA6BSDYbrDesEK1rVJp5nvmL3a+2uszG8ZEwEHZ+h2zb5kMnAOt2PYkEKVkUc52SEiuaef8gnLhvLg522HlU4BFhEz5AGmWXcfvBXw4xNduJL7qhxUFm35sRqAVBNYpAJ2TWURIodxICiSeFmJpm42V212ymGXT709tiIUiGhqQ05nDIthQdA67sCoJgEBr4ZaUvVEngNUHfpdoDAh5nDOl8O5yBxmR+yFPp5ZUYih8jDyuqvc05Sq4RiEemxW7f1WV67hkudjY874jsE4BqC7BsIAvxGVMa6QiJgrK5J3kHKaBfsmSJ3GWwPPrxXgUoRZDYMA+VDHmUG4u1FJpDertXLExgJ9uBNZEX+YluyKWCizeK5d1qMeEeCilNiBVTGmRgHaIJJ6rS87/YTNl9758bRBYkwAUOSOpWUJL1NSa5TlKeSd8rbMFgnLBsJHHw8uV5EdYSgu0yHZO40ykKVm0wG5aaSpCNyd4IgSKlAwBvDYCDwSLtw/mP7XiSIeNRlS33zDkAInXz5qOwRefvrGVJ56vy0iBKsE2I7BwBNYlADXrrPSQfJDOdu4vaZ8cAIbnhRcA+TsvlAJbiS1ZJGkVVYTabjGoHNFStzM1myjnnOcglFnnOg239O2N3Ke04oFgsCY2opzAuKdgNQ48B+QB/XBNgPMwl4zAugSgh1ZZ7MdtYNuXr/pI6QOCzr0zfRpW27Dx5U4CKYKQlJJo2SIF02CB1QaNG0ZUTyxeNxMwHrjJtiR+fm1jdZhUqHtGn14WjClfiI+JJsOPziFaPFkKkC62sFQq/DX5gmtI4avL8tz9rBcC6xKAml/2T5LC87ub5pTsCJONLbbm+UTtEkQCEh45o3j/cr/cRyR5ehCA4pWzFA5qFU26eXMKv7BRSJD6RpsVeutuxrvreaTybR2Enrp14mr841xKgut3lQJlw21GYOEIrEkAAreBbYkliW2mF/plqy/ym0m6wxBTqTmZECTI3zlZ0klSQPKlAxsJQB1RsdPqkHOwAzPVeBwWvBT5okeUEyagbSwCSdcN2vkejV9TagEJqk1yhgZywnaWAiUmbEZg4QisUwDi4J5s99r2kQLbWwu0xEbiEJyttSYtdrM+ELVktfPqds92phniQKDuIADFvSoqOMUDa+HP2nlWhpQIoFYRBIy6YNo0g1vTa5P2IUGIvKHaOB97kBTquj+NX8VmzhzluFlh2ozAwhFYowAEdjvKDPOPL2/k1syr75VWDWhzkeRIANppglmCycYW2yn9E3ocCEBLlCtK20XQzncZwg9JIYLOsVJglWibCYGx50JZPSGpPjQER+MtK0IMzyhGztwSzh9nGpRvKhyBdQtAzVLDTN2MStiJ7so5DiudSQ3ZfEo0I2q5QOHISbuf9LqIAClq2tgXpDCOHDFpYxNclwIPW23DlHOIBNQrapH1N4F7K3VJCkLoyLHKbNoQQy6iEvGJfMGNHfhX6iHo1WDWLACBfYQNx754bQQGNOL42t+CHZRelFud44ybWNpmn53yBoXozkW2W2CbYR+utmggTbRgmzK1+PJ+usuCtwB5anJT3BsJ2kmglfIcWA/TFtrCxu0uG4F1DEB8BVKSe5zGFecK51UMta9UiYvXlgQteVItLDTEPrKhIMDXYxuFwpbxeOxQC6ezlrTA0kuQpNemlh0Jk48vF7dlzM2i+khnQkg7NYkJNJ6DUCo5Xp+xLYmws6ixuZ2SEFjDAJS+AofzXWadM/TkYGBRjhkR0BXIGG+ThJDYVVT2pPRD05CMgZZuWyoCAx8bdc+wD1mt11ufCyzzsNTBubMCEFjTAJSCEF+BD8+stWmnij9QWHWvl0K9VTFtGz28fqQkAwoCKA3MaRE9O7b1njamoZ5LI8059F7fPjZPCI9Rj6jL1TsA9XoOy3ZujQNQCkJsAXEmxMtxM+XnX2YBy+9nEgHUZM52VtAiVF0o0LXNWbYiBXnEMG8zBizOxrZJiFxBiHs3pPjIvILfzLMeyzH1Dkw7NCUCax6AmmjFy0m6bSVYCQWVsgSoCX+pv1poU870RJePBKAZXj7pfIfAA0twnJgqJRAQBYVabesFAulDYbiuUO3ZDM9ALwZlJwpAwAGogElanouRRMVmbZxDpfCs7fuPu2b5fr6mNzNKoR8phZdu356vWD4CkZUvK+Bh8xbc8idjbXp0AFqbqZ5koJFibcgA1Rp1J0ihSVlvNJJKXFA3hmvJnN/MyCdB2BIq+ncm8cLXdIXAWBXtJeSCdTVe99s1Ag5AXc9A7/qPJ1UJqA/IbkG2oDzDCbmSKKy/q2cK7wHbuI7METk9JJNazr9387yZQ8M1odJ16O3Vz0QxI7Gj/UfAAaj/c7RkD9N2GvlMzRydaXygkFnP1R6mGc66XZvO8N5cbZdeYWjkyEZNo/yxbsB5vDMg4AA0A2irf8uIKvZ2Qz431yw6a7sL/fsSEBhRC6mdfslsJb5LGLN97AIBB6AuUC+iz0h5a9TDt1L1zjJDw2KWRQzQTm6KQKR8CMoU46wh2WMIjcB8CDgAzYffGtydKLqHZbIB5SMunSnqz63ye1BRRoLItlIIDCQkQ0pp0unPl8ItVmq4HkxnCDgAdQa9OzYCfUUgFbHLJeyTUnldnhuHf7SRuF2KuG5fMbZfIOAA5OfACBiBIQQiOnD11iukElbADQt+b/iZWQgCfpAWAqMbMQKrhEBkW/XKkn6Wt1ybg3tLVR9ov1UarcfSHQIOQN1h756NQE8RGJuQiq8HSTpNCuSD2YzA3Ag4AM0NoRswAquGwMAWHIODEUf14GNWbaQeT7cIOAB1i797NwI9RSDR8KnRhPr70T110m4VjoADUOETaPeNgBEwAqUi4ABU6szZbyNgBIxA4Qg4ABU+gXbfCBgBI1AqAg5Apc6c/TYCRsAIFI6AA1DhE2j3jYARMAKlIuAAVOrM2W8jYASMQOEIOAAVPoF23wgYASNQKgIOQKXOnP02AkbACBSOgANQ4RNo942AETACpSLgAFTqzNlvI2AEjEDhCDgAFT6Bdt8IGAEjUCoCDkClzpz9NgJGwAgUjoADUOETaPeNgBEwAqUi4ABU6szZbyNgBIxA4Qg4ABU+gXbfCBgBI1AqAg5Apc6c/TYCRsAIFI6AA1DhE2j3jYARMAKlIuAAVOrM2W8jYASMQOEIOAAVPoF23wgYASNQKgIOQKXOnP02AkbACBSOgANQ4RNo942AETACpSLgAFTqzNlvI2AEjEDhCDgAFT6Bdt8IGAEjUCoCDkClzpz9NgJGwAgUjoADUOETaPeNgBEwAqUi4ABU6szZbyNgBIxA4Qg4ABU+gXbfCBgBI1AqAg5Apc6c/TYCRsAIFI6AA1DhE2j3jYARMAKlIuAAVOrM2W8jYASMQOEIOAAVPoF23wgYASNQKgIOQKXOnP02AkbACBSOgANQ4RNo942AETACpSLgAFTqzNlvI2AEjEDhCDgAFT6Bdt8IGAEjUCoCDkClzpz9NgJGwAgUjoADUOETaPeNgBEwAqUi4ABU6szZbyNgBIxA4Qg4ABU+gXbfCBgBI1AqAg5Apc6c/TYCRsAIFI6AA1DhE2j3jYARMAKlIuAAVOrM2W8jYASMQOEIOAAVPoF23wgYASNQKgIOQKXOnP02AkbACBSOgANQ4RNo942AETACpSLgAFTqzNlvI2AEjEDhCDgAFT6Bdt8IGAEjUCoC/w/4TfjiN6lu3AAAAABJRU5ErkJggg=="

	// Extract the Base64-encoded part
	base64Image := strings.Split(base64ImageURI, ",")[1]

	// Decode Base64 to raw binary data
	rawData, err := base64.StdEncoding.DecodeString(base64Image)
	if err != nil {
		fmt.Println("Error decoding Base64:", err)
		return
	}

	// Example encryption key
	encryptionKey := []byte("my_secret_key_16")

	// Encrypt the data using AES
	encryptedData, err := encryptAES(rawData, encryptionKey)
	if err != nil {
		fmt.Println("Error encrypting data:", err)
		return
		
	}

	// Decrypt the data using AES
	decryptedData, err := decryptAES(encryptedData, encryptionKey)
	if err != nil {
		fmt.Println("Error decrypting data:", err)
		return
	}

	// Continue with further processing if needed...
	fmt.Println("Original Data:", rawData)
	fmt.Println("Encrypted Data:", encryptedData)
	fmt.Println("Decrypted Data:", decryptedData)
}

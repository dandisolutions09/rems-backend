package main

import (
	"encoding/json"
	"net/http"

	"gorm.io/gorm"
)

// "log"

// Define a sample model (table structure)
// type UserS struct {
// 	ID   uint   `gorm:"primaryKey"`
// 	Name string `gorm:"column:name"`
// 	Age  int    `gorm:"column:age"`
// }

type UserS struct {
	ID         uint   `gorm:"primaryKey"`
	Name       string `gorm:"column:name"`
	Department string `gorm:"column:department"`
	Wards      []Ward `gorm:"many2many:user_wards;"`
	Grade              string `gorm:"column:grade"`
	SignatureImageData string `gorm:"column:signatureImageData"`
}


type Owner []string
type Item struct {
 gorm.Model
 Name  string
 Owner Owner `gorm:"type:VARCHAR(255)"` // Use custom datatype
}

type Ward struct {
	ID   uint   `gorm:"primaryKey"`
	Name string `gorm:"column:name"`
	// ... other Ward fields
}

func createUser(w http.ResponseWriter, r *http.Request) {
	var user UserS

	// Check if the db variable is nil
	if db == nil {
		http.Error(w, "Database connection is nil", http.StatusInternalServerError)
		return
	}

	// Decode the incoming JSON data into the User struct
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Create the user in the database
	if err := db.Create(&user).Error; err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the created user as JSON
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

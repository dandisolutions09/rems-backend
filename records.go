package main

import (
	// "container/list"
	"context"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"sort"

	//"sort"

	// "strconv"
	"time"

	// "time"

	// "time"

	//"github.com/gorilla/handlers"
	// "github.com/gorilla/mux"
	// "go.mongodb.org/mongo-driver/bson"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo"
	// "gorm.io/gorm"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

type Installments struct {
	Amount          string `json:"amount" bson:"amount"`
	InstallmentType string `json:"installment_type" bson:"installment_type"`
	CreatedAt       string `json:"created_at" bson:"created_at"`
}

//BonusPayments

type BonusPayments struct {
	Amount          string `json:"amount" bson:"amount"`
	InstallmentType string `json:"installment_type" bson:"installment_type"`
	CreatedAt       string `json:"created_at" bson:"created_at"`
}

type Client_Information struct {
	First_name  string `json:"first_name" bson:"first_name"`
	Second_name string `json:"second_name" bson:"second_name"`
	Last_name   string `json:"last_name" bson:"last_name"`
	KRA_pin     string `json:"kra_pin" bson:"kra_pin"`
	Email       string `json:"email" bson:"email"`
	PhoneNumber string `json:"phone_number" bson:"phone_number"`
	NationalID  string `json:"nat_id" bson:"nat_id"`
	CreatedAt   string `json:"created_at" bson:"created_at"`
	HouseStatus string `json:"house_status" bson:"house_status"` //Booked, Interested, Taken, Rejected, Cancelled, if Boooked, they have deposited 100k

}

type Broker_Information struct {
	First_name    string `json:"first_name" bson:"first_name"`
	Last_name     string `json:"last_name" bson:"last_name"`
	Company_name  string `json:"company_name" bson:"company_name"`
	Company_Email string `json:"company_email" bson:"company_email"`
	PhoneNumber   string `json:"phone_number" bson:"phone_number"`
	CreatedAt     string `json:"created_at" bson:"created_at"`
}

type House_Information struct {
	Location     string `json:"location" bson:"location"`
	Residence    string `json:"residence" bson:"residence"`
	HouseType    string `json:"house_type" bson:"house_type"`
	HouseNumber  string `json:"house_number" bson:"house_number"`
	AskingPrice  string `json:"asking_price" bson:"asking_price"`
	SellingPrice string `json:"selling_price" bson:"selling_price"`
	CreatedAt    string `json:"created_at" bson:"created_at"`
}

type Record struct {
	ID     string `json:"_id,omitempty" bson:"_id,omitempty"`
	Status string `json:"status" bson:"status"`
	// User_Name         string              `json:"user_name" bson:"user_name"`
	Client_Information Client_Information  `json:"client_information" bson:"client_information"`
	House_Information  []House_Information `json:"house_information" bson:"house_information"` // one client can have multiple houses
	Broker_Information Broker_Information  `json:"broker_information" bson:"broker_information"`
	Installments       []Installments      `json:"installments" bson:"installments"`
	BonusPayments      []BonusPayments     `json:"bonus_payments" bson:"bonus_payments"`

	Date      string `json:"date" bson:"date"` // date of the agreement with the client
	CreatedAt string `json:"created_at" bson:"created_at"`
}

type Usr struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	First_name  string             `json:"first_name" bson:"first_name"`
	Second_name string             `json:"second_name" bson:"second_name"`
	Last_name   string             `json:"last_name" bson:"last_name"`
	Username    string             `json:"username" bson:"username"`
	Email       string             `json:"email" bson:"email"`
	Password    string             `json:"password" bson:"password"`
	Salt        []byte             `json:"salt" bson:"salt"`
	Role        string             `json:"role" bson:"role"`
	Records     []Record           `json:"records" bson:"records"`
	Status      string             `json:"status" bson:"status"`
	CreatedAt   string             `json:"created_at" bson:"created_at"`
}

type DeactivateStruct struct {
	Status string `json:"status" bson:"status"`
}

// Custom type to implement sorting for Records slice
type RecordSlice []Record

func (r RecordSlice) Len() int {
	return len(r)
}

func (r RecordSlice) Less(i, j int) bool {
	// Convert createdAt strings to time.Time for comparison
	timeI, _ := time.Parse(time.RFC3339, r[i].CreatedAt)
	timeJ, _ := time.Parse(time.RFC3339, r[j].CreatedAt)

	// Compare based on createdAt property in reverse order (latest first)
	return timeI.After(timeJ)
}

func (r RecordSlice) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

//////////////////////////////////////////////////////////////////

func handleAddRec(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Usr
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	currentTime := time.Now()

	if currentTime.Location().String() != "Africa/Nairobi" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Africa/Nairobi")
		if err != nil {
			fmt.Println("Error loading Nairobi location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		updatedData.CreatedAt = timeString

		fmt.Println("incoming record", updatedData)

		// Update the specific records in MongoDB
		collection := client.Database(dbName).Collection(records_collection)
		filter := bson.M{"_id": objID}
		///update := bson.M{"$set": updatedData}

		update := bson.M{
			"$set": bson.M{
				//"status": updatedData.Status,
				// "client_information": updatedData.Client_Information,
				// "house_information":  updatedData.House_Information,
				// "broker_information": updatedData.Broker_Information,
				"records": updatedData.Records,
				// Include all other fields you want to update, excluding 'created_at'
			},
		}

		_, err = collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, "Error updating nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		//fmt.Fprintf(w, "Facility updated successfully")

		response := map[string]string{"message": "Record updated successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {

	}

}

type LoginResponse struct {
	Message string `json:"message"`
	Token   string `json:"token,omitempty"`
	User    Usr    `json:"user,omitempty"`
	Status  string `json:"status,omitempty"`
}



// Define your handler function to retrieve all records

func handleLogin(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var u User
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Make a GET request to retrieve the stored hash from another endpoint
	url := "http://localhost:8080/get-username/" + u.Username
	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Error making GET request", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	storedHash, err := io.ReadAll(response.Body)
	if err != nil {
		http.Error(w, "Error reading response body", http.StatusInternalServerError)
		return
	}

	var storedUser Usr
	if err := json.Unmarshal(storedHash, &storedUser); err != nil {
		http.Error(w, "Error parsing JSON response", http.StatusInternalServerError)
		return
	}

	passwordsMatch := comparePasswords(u.Password, storedUser.Password, storedUser.Salt)
	if passwordsMatch {
		createToken(u.Username)

		response := LoginResponse{
			Message: "Authentication successful",
			Token:   "your_generated_token",
			User:    storedUser,
			//Status:  storedUser.Status,
		}
		json.NewEncoder(w).Encode(response)
	} else {
		//http.Error(w, "Authentication failed", http.StatusUnauthorized)
		response := LoginResponse{
			Message: "Authentication Not Successful",
			//Token:   "your_generated_token",
			//User:    storedUser,
			//Status:  storedUser.Status,
		}
		json.NewEncoder(w).Encode(response)
	}
}


func getAllRecordsHandler(w http.ResponseWriter, r *http.Request) {
    // Set up MongoDB client
   // clientOptions := options.Client().ApplyURI(mongoURI)
    // client, err := mongo.Connect(context.Background(), clientOptions)
    // if err != nil {
    //     log.Fatal(err)
    // }
    // defer client.Disconnect(context.Background())

    // Access your collection
	collection := client.Database(dbName).Collection("new_users")

    // Define a slice to hold the results
    var records []Usr

    // Fetch all documents from the collection
    cursor, err := collection.Find(context.Background(), bson.M{})
    if err != nil {
        log.Fatal(err)
    }
    defer cursor.Close(context.Background())

    // Iterate over the cursor and decode each document into a Record struct
    for cursor.Next(context.Background()) {
        var record Usr
        if err := cursor.Decode(&record); err != nil {
            log.Fatal(err)
        }
        records = append(records, record)
    }
    if err := cursor.Err(); err != nil {
        log.Fatal(err)
    }

    // Marshal the records to JSON and send the response
    jsonBytes, err := json.Marshal(records)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    // Set the content type header and write the JSON response
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
    w.Write(jsonBytes)
}


func handleGetUserByID(w http.ResponseWriter, r *http.Request) {
	// Parse user ID from request URL
	userID := mux.Vars(r)["id"]

	// Convert userID to ObjectID
	objID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		http.Error(w, "Invalid user ID", http.StatusBadRequest)
		return
	}

	// Access the "new_users" collection
	collection := client.Database(dbName).Collection("new_users")

	// Define filter to retrieve the user by ID
	filter := bson.M{"_id": objID}

	// Define a variable to store the retrieved user
	var user Usr

	// Retrieve the user from the collection
	err = collection.FindOne(context.Background(), filter).Decode(&user)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			http.Error(w, "User not found", http.StatusNotFound)
			return
		}
		http.Error(w, "Error retrieving user", http.StatusInternalServerError)
		return
	}

	// Sort the Records array based on createdAt property (latest first)
	sort.Sort(RecordSlice(user.Records))

	// Respond with the retrieved user in JSON format
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, "Error encoding user data", http.StatusInternalServerError)
		return
	}
}

// func handleGetUsrByID(w http.ResponseWriter, r *http.Request) {

// 	fmt.Print("get driver by ID has been called....")
// 	// Get the driver ID from the request parameters
// 	params := mux.Vars(r)
// 	driverID := params["id"]

// 	// Convert the Driver ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(driverID)
// 	if err != nil {
// 		http.Error(w, "Invalid Driver ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve the specific driver from MongoDB
// 	collection := client.Database(dbName).Collection(new_users)
// 	var driver Usr
// 	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&driver)
// 	if err != nil {
// 		//http.Error(w, "Driver not found", http.StatusNotFound)
// 		response := map[string]string{"error": "Facility not found"}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}

// 	fmt.Println("Driver:--->", driver)

// 	// Respond with the retrieved driver in JSON format
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(driver)
// }

func handleGetUserByName(w http.ResponseWriter, r *http.Request) {

	fmt.Println("get driver by ID has been called....")
	// Get the driver ID from the request parameters
	params := mux.Vars(r)
	username := params["username"]

	// Convert the Driver ID to a MongoDB ObjectID

	// Retrieve the specific driver from MongoDB
	collection := client.Database(dbName).Collection("new_users")
	var usr User
	err := collection.FindOne(context.Background(), bson.M{"username": username}).Decode(&usr)
	if err != nil {
		//http.Error(w, "Driver not found", http.StatusNotFound)
		response := map[string]string{"error": "Facility not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved driver in JSON format
	w.Header().Set("Content-Type", "application/json")

	//passwordVerification()

	json.NewEncoder(w).Encode(usr)
}

func isEmailTaken(email string) bool {
	// Implement logic to check if the email already exists in the database
	// Example:
	collection := client.Database(dbName).Collection("new_users")
	count, err := collection.CountDocuments(context.Background(), bson.M{"email": email})
	if err != nil {
		fmt.Println("Error checking email:", err)
		return true // Assume email is taken in case of error
	}
	return count > 0
}

func isUsernameTaken(username string) bool {
	// Implement logic to check if the username already exists in the database
	// Example:
	collection := client.Database(dbName).Collection("new_users")
	count, err := collection.CountDocuments(context.Background(), bson.M{"username": username})
	if err != nil {
		fmt.Println("Error checking username:", err)
		return true // Assume username is taken in case of error
	}
	return count > 0
}

func RegisterNewUser(w http.ResponseWriter, r *http.Request) {

	fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	var inc_data Usr
	err := decoder.Decode(&inc_data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("incoming data", inc_data)

	// Check if the username or email is taken
	if isUsernameTaken(inc_data.Username) {
		http.Error(w, "Username-already-taken", http.StatusBadRequest)
		return
	}

	if isEmailTaken(inc_data.Email) {
		http.Error(w, "Email-already-taken", http.StatusBadRequest)
		return
	}

	salt := make([]byte, 16)
	rsp, err := rand.Read(salt)
	if err != nil {
		fmt.Println("Error generating salt:", err)
		return
	}

	fmt.Println("salting resp:", rsp)

	// Hash the password with the generated salt
	hashedPassword, salt, err := hashPassword(inc_data.Password, salt)
	if err != nil {
		fmt.Println("Error hashing password:", err)
		return
	}

	// fmt.Println("Password:", inc_data.Password)
	// fmt.Println("Hashed Password:", hashedPassword)
	// fmt.Printf("Salt: %x\n", salt)

	// Unmarshal JSON string into a struct (using a pointer to the variable)

	currentTime := time.Now()

	if currentTime.Location().String() != "Africa/Nairobi" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Europe/London")
		if err != nil {
			fmt.Println("Error loading London location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		inc_data.CreatedAt = timeString

		inc_data.Password = hashedPassword
		inc_data.Salt = salt

		// fmt.Println("to be saved in db:->", inc_data)
		// fmt.Println("FULLNAME", inc_data.First_name+" "+inc_data.Last_name)
		fmt.Println("FULLNAME:", inc_data.First_name+" "+inc_data.Last_name)

		collection := client.Database(dbName).Collection("new_users")
		_, err = collection.InsertOne(context.Background(), inc_data)
		fmt.Print(inc_data)
		if err != nil {
			http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		// fmt.Fprintf(w, "Nurse inserted Successfully")

		response := map[string]string{"message": "User inserted successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {
		fmt.Println("Current Time is already in Africa/Nairobi time zone.")
	}

	//timeString := currentTime.Format("2006-01-02 15:04:05")
	//fmt.Println("Current time as string:", timeString)

}

func handleAddRecord(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	//fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	var data Record
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("inc house information", data.House_Information)

	for i := 0; i < len(data.House_Information); i++ {
		fmt.Println(data.House_Information[i].Location, data.House_Information[i].Residence)
	}

	currentTime := time.Now()

	if currentTime.Location().String() != "Africa/Nairobi" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Africa/Nairobi")
		if err != nil {
			fmt.Println("Error loading Nairbi location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		data.CreatedAt = timeString

		//data.CreatedAt = "2023-01-02 15:04:05" //EXPIRY TEST
		//data.CreatedAt = "2023-11-22 16:04:05" //AMBER TEST

		//		November 22, 2023.

		// fmt.Println("to be saved in db:->", inc_data)

		collection := client.Database(dbName).Collection(records_collection)
		_, err = collection.InsertOne(context.Background(), data)
		fmt.Print(data)
		if err != nil {
			http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		// fmt.Fprintf(w, "Nurse inserted Successfully")

		response := map[string]string{"message": "Record inserted successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {
		fmt.Println("Current Time is already in Africa/Nairobi time zone.")
	}

}

// func handleGetRecords(w http.ResponseWriter, r *http.Request) {
// 	collection := client.Database(dbName).Collection("new_users")

// 	// Define a slice to store retrieved records
// 	var records []Usr

// 	// Define a filter to retrieve only active records
// 	filter := bson.M{"status": "active"}

// 	// Retrieve documents from the collection that match the filter
// 	cursor, err := collection.Find(context.Background(), filter)
// 	if err != nil {
// 		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
// 		return
// 	}
// 	defer cursor.Close(context.Background())

// 	// Iterate through the cursor and decode documents into the records slice
// 	for cursor.Next(context.Background()) {
// 		var record Usr
// 		if err := cursor.Decode(&record); err != nil {
// 			http.Error(w, "Error decoding record", http.StatusInternalServerError)
// 			return
// 		}
// 		records = append(records, record)
// 	}

// 	// Respond with the retrieved records in JSON format
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(records)
// }

func handleGetRecords(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection("new_users")

	fmt.Println("Get all records called")

	// Define a slice to store retrieved records
	var records []Usr

	// Define a filter to retrieve only active records
	filter := bson.M{"status": "ACTIVE"}

	// Retrieve documents from the collection that match the filter
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error retrieving records", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the records slice
	for cursor.Next(context.Background()) {
		var record Usr
		if err := cursor.Decode(&record); err != nil {
			http.Error(w, "Error decoding record", http.StatusInternalServerError)
			return
		}
		records = append(records, record)
	}

	// Respond with the retrieved records in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(records)
}

func handleUpdateRecord(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Record
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	currentTime := time.Now()

	if currentTime.Location().String() != "Africa/Nairobi" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Africa/Nairobi")
		if err != nil {
			fmt.Println("Error loading Nairobi location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		updatedData.CreatedAt = timeString

		// Update the specific records in MongoDB
		collection := client.Database(dbName).Collection(records_collection)
		filter := bson.M{"_id": objID}
		///update := bson.M{"$set": updatedData}

		update := bson.M{
			"$set": bson.M{
				// "agent_name":         updatedData.Agent_Name,
				"client_information": updatedData.Client_Information,
				"house_information":  updatedData.House_Information,
				"broker_information": updatedData.Broker_Information,
				"installments":       updatedData.Installments,
				// Include all other fields you want to update, excluding 'created_at'
			},
		}

		_, err = collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, "Error updating nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		//fmt.Fprintf(w, "Facility updated successfully")

		response := map[string]string{"message": "Record updated successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {

	}

}

func handleDeactivateRecord(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Record
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	currentTime := time.Now()

	if currentTime.Location().String() != "Africa/Nairobi" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Africa/Nairobi")
		if err != nil {
			fmt.Println("Error loading Nairobi location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		updatedData.CreatedAt = timeString

		// Update the specific records in MongoDB
		collection := client.Database(dbName).Collection(records_collection)
		filter := bson.M{"_id": objID}
		///update := bson.M{"$set": updatedData}

		update := bson.M{
			"$set": bson.M{
				"status": updatedData.Status,
				// "client_information": updatedData.Client_Information,
				// "house_information":  updatedData.House_Information,
				// "broker_information": updatedData.Broker_Information,
				// "installments":       updatedData.Installments,
				// Include all other fields you want to update, excluding 'created_at'
			},
		}

		_, err = collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, "Error updating nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		//fmt.Fprintf(w, "Facility updated successfully")

		response := map[string]string{"message": "Record updated successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {

	}

}


func handleRestoreRecord(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Record
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	currentTime := time.Now()

	if currentTime.Location().String() != "Africa/Nairobi" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Africa/Nairobi")
		if err != nil {
			fmt.Println("Error loading Nairobi location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		updatedData.CreatedAt = timeString

		// Update the specific records in MongoDB
		collection := client.Database(dbName).Collection(records_collection)
		filter := bson.M{"_id": objID}
		///update := bson.M{"$set": updatedData}

		update := bson.M{
			"$set": bson.M{
				"status": updatedData.Status,
				// "client_information": updatedData.Client_Information,
				// "house_information":  updatedData.House_Information,
				// "broker_information": updatedData.Broker_Information,
				// "installments":       updatedData.Installments,
				// Include all other fields you want to update, excluding 'created_at'
			},
		}

		_, err = collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, "Error updating nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		//fmt.Fprintf(w, "Facility updated successfully")

		response := map[string]string{"message": "Record updated successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {

	}

}

func handlePostRecord(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Post record!")
	params := mux.Vars(r)
	facilityID := params["id"]

	fmt.Println("ID", facilityID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Record

	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	fmt.Println("updatedData", updatedData)
	fmt.Println("DON COLLINS----")

	// Update the specific record in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID}
	update := bson.M{"$push": bson.M{"records": updatedData}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}

func handlePostInstallment(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Post installment!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// // Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Installments

	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	fmt.Println("updatedData", updatedData)
	fmt.Println("DON COLLINS----")

	// Update the specific record in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$push": bson.M{"records.$.installments": updatedData}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handlePostPayment(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Post installment!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// // Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Installments

	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	fmt.Println("updatedData", updatedData)
	fmt.Println("DON COLLINS----")

	// Update the specific record in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$push": bson.M{"records.$.bonus_payments": updatedData}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleUpdateRecordStatus(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", recordID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var updatedStatus struct {
		Status string `json:"status"`
	}

	err = decoder.Decode(&updatedStatus)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$set": bson.M{"records.$.status": "INACTIVE"}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record status updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}


func handleRestoreRecordStatus(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", recordID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var updatedStatus struct {
		Status string `json:"status"`
	}

	err = decoder.Decode(&updatedStatus)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$set": bson.M{"records.$.status": "ACTIVE"}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record status updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// handleCancelRecordStatus
func handleCancelRecordStatus(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", recordID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var updatedStatus struct {
		Status string `json:"status"`
	}

	err = decoder.Decode(&updatedStatus)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$set": bson.M{"records.$.client_information.house_status": "CANCELLED"}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record status updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

///handleCancelRecordStatus

func handleBookRecordStatus(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", recordID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var updatedStatus struct {
		Status string `json:"status"`
	}

	err = decoder.Decode(&updatedStatus)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$set": bson.M{"records.$.client_information.house_status": "BOOKED"}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record status updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//handleFullySoldRecordStatus

func handleFullySoldRecordStatus(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", recordID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var updatedStatus struct {
		Status string `json:"status"`
	}

	err = decoder.Decode(&updatedStatus)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$set": bson.M{"records.$.client_information.house_status": "FULLY-SOLD"}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record status updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleChangeToInterestedtatus(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", recordID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var updatedStatus struct {
		Status string `json:"status"`
	}

	err = decoder.Decode(&updatedStatus)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$set": bson.M{"records.$.client_information.house_status": "INTERESTED"}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record status updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleSoldRecordStatus(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", recordID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var updatedStatus struct {
		Status string `json:"status"`
	}

	err = decoder.Decode(&updatedStatus)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$set": bson.M{"records.$.client_information.house_status": "SOLD"}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record status updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleFullySoldStatus(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record index from the request parameters
	fmt.Println("Update Record Status!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	fmt.Println("record id", recordID)

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Convert the record ID to a MongoDB ObjectID
	// recordObjID, err := primitive.ObjectIDFromHex(recordID)
	// if err != nil {
	// 	http.Error(w, "Invalid Record ID", http.StatusBadRequest)
	// 	return
	// }

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var updatedStatus struct {
		Status string `json:"status"`
	}

	err = decoder.Decode(&updatedStatus)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("error", err)
		return
	}

	// Update the status of the record at the specified index in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}
	update := bson.M{"$set": bson.M{"records.$.client_information.house_status": "FULLY-SOLD"}}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("errr", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record status updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleAddInstallment(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Record
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	currentTime := time.Now()

	if currentTime.Location().String() != "Africa/Nairobi" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Africa/Nairobi")
		if err != nil {
			fmt.Println("Error loading Nairobi location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		updatedData.CreatedAt = timeString

		// Update the specific records in MongoDB
		collection := client.Database(dbName).Collection(records_collection)
		filter := bson.M{"_id": objID}
		///update := bson.M{"$set": updatedData}

		update := bson.M{
			"$set": bson.M{
				//"status": updatedData.Status,
				// "client_information": updatedData.Client_Information,
				// "house_information":  updatedData.House_Information,
				// "broker_information": updatedData.Broker_Information,
				"installments": updatedData.Installments,
				// Include all other fields you want to update, excluding 'created_at'
			},
		}

		_, err = collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, "Error updating nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		//fmt.Fprintf(w, "Facility updated successfully")

		response := map[string]string{"message": "Record updated successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {

	}

}

func handleUpdateAllRecords(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	fmt.Println("Handle update is called")
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Record
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	currentTime := time.Now()

	if currentTime.Location().String() != "Africa/Nairobi" {
		// Load the "Europe/London" time zone
		fmt.Println("time is not London Time")
		londonLocation, err := time.LoadLocation("Africa/Nairobi")
		if err != nil {
			fmt.Println("Error loading Nairobi location:", err)
			return
		}

		// Convert the current time to "Europe/London" time zone
		currentTime = currentTime.In(londonLocation)
		fmt.Println("Current Time (After):", currentTime)

		timeString := currentTime.Format("2006-01-02 15:04:05")

		updatedData.CreatedAt = timeString

		// Update the specific records in MongoDB
		collection := client.Database(dbName).Collection(records_collection)
		filter := bson.M{"_id": objID}
		///update := bson.M{"$set": updatedData}

		update := bson.M{
			"$set": bson.M{
				// "agent_name":         updatedData.Agent_Name,
				"client_information": updatedData.Client_Information,
				"house_information":  updatedData.House_Information,
				"broker_information": updatedData.Broker_Information,
				"installments":       updatedData.Installments,
				// Include all other fields you want to update, excluding 'created_at'
			},
		}

		_, err = collection.UpdateOne(context.Background(), filter, update)

		if err != nil {
			http.Error(w, "Error updating nurse", http.StatusInternalServerError)
			return
		}

		// Respond with a success message
		//fmt.Fprintf(w, "Facility updated successfully")

		response := map[string]string{"message": "Record updated successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

	} else {

	}

}

func handleEditRecord(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID and record ID from the request parameters
	fmt.Println("handle edit has been called!!")
	params := mux.Vars(r)
	facilityID := params["id"]
	recordID := params["recordID"] // Assuming you have a parameter called "recordID" for specifying the record ID

	// Convert the facility ID and record ID to MongoDB ObjectIDs
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid Record ID", http.StatusBadRequest)
		return
	}

	//fmt.Println("decoder-->", r.Body)

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)

	defer r.Body.Close()

	var updatedRecord Record

	fmt.Println("updatdRecord-->", updatedRecord)
	err = decoder.Decode(&updatedRecord)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		fmt.Println("err", err)
		return
	}

	fmt.Println("updated record ID", recordID, facilityID)

	// Update the specific record in MongoDB
	collection := client.Database(dbName).Collection("new_users")
	filter := bson.M{"_id": objID, "records._id": recordID}

	update := bson.M{
		"$set": bson.M{
			"records.$": updatedRecord,
		},
	}

	//fmt.Println("update", update)

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating record", http.StatusInternalServerError)
		fmt.Println("err", err)
		return
	}

	// Respond with a success message
	response := map[string]string{"message": "Record updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// ADD NEW PROPERTY
func AddPropertyHandler(client *mongo.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Access your collection
		collection := client.Database("test-prop-db").Collection("collection-prop")

		// Define the property you want to add
		newProperty := "new value"

		// Define a filter to match all documents
		filter := bson.D{}

		// Define an update to add the property
		update := bson.D{
			{"$set", bson.D{
				{"property", newProperty},
			}},
		}

		// Update all documents matching the filter
		_, err := collection.UpdateMany(context.TODO(), filter, update)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Respond with success message
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Property added to all documents successfully"))
	}
}
